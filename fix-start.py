#!/usr/bin/env python3
import os
import sys
import json
import math

if len(sys.argv) != 2:
	print('''Usage: %s directory
	The directory will be recursively searched for files ending in '.recording.json'.
	Each file is then checked for eligibility and fixed if necessary.
	Check the rules at the start of this script for more info.''' % (sys.argv[0]))
	exit()

name, rootdir = sys.argv

max_movement_time = 1


for root, subdirectories, files in os.walk(rootdir):
	for filename in files:

		if not filename.endswith('.recording.json'):
			continue

		path = os.path.join(root, filename)

		f = open(path, 'r')
		data = json.load(f)
		f.close()

		cubes = data['scene']

		for cube in cubes:

			if len(cube['data']) > 1 and cube['data'][0]['time'] == 0.0 and cube['data'][len(cube['data']) - 1]['time'] <= 1.0:
				print('eligible: %f to %f in %s' %(cube['data'][0]['time'], cube['data'][len(cube['data']) - 1]['time'], path))

				cube['data'] = cube['data'][0]

		# write back to file
		f = open(path, 'w')
		json.dump(data, f, indent=1)
		f.close()
