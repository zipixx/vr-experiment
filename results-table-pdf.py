#!/usr/bin/env python3
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages


pdf_pages = PdfPages('/tmp/vr.pdf')
subjectCount = 20


fig, ax = plt.subplots()
ax.axis('off')


clust_data = np.random.random((subjectCount,3))
rowlabel = [x+10 for x in range(1,subjectCount+1)]
collabel=("col 1", "col 2", "col 3")
ax.table(cellText=clust_data,rowLabels=rowlabel,colLabels=collabel,loc='center', bbox=None)




pdf_pages.savefig(fig)
pdf_pages.close()
