#!/usr/bin/env python3
import os
import sys
import json
import math

if len(sys.argv) != 2:
	print('''Usage: %s directory
	The directory will be recursively searched for files ending in '.recording.json'.
	Each file is then checked for eligibility and fixed if necessary.
	Check the rules at the start of this script for more info.''' % (sys.argv[0]))
	exit()

name, rootdir = sys.argv

# eligibility rules. change if need be
despawned_cube_max_movement = 10  # max distance in cm to be allowed for hand movement in any dimension -> if below, it will be cut out
spawned_cube_non_static_time = 0.25  # if recording of spawned cube less, delete all but first data-point
spawned_cube_max_distance = 50  # max distance in cm between spawn and despawn points
spawned_cube_max_time_difference = 1.5  # cube needs to be spawned in a +/- seconds window around despawn time to be eligible


# remove hand movement jitter: replace despawned cubes data-points with: start, immediately before despawn (necessary because of location interpolation in rendering) and finally despawned.
def fix_despawned_cube(cube):
	cube['data'] = [cube['data'][0], cube['data'][len(cube['data']) - 2], cube['data'][len(cube['data']) - 1]]


# fix time difference and make cube static if <250ms recorded
def fix_spawned_cube(cube, delta):
	if cube['data'][len(cube['data']) - 1]['time'] - cube['data'][0]['time'] < spawned_cube_non_static_time:
		cube['data'] = cube['data'][:1]
	for point in cube['data']:
		point['time'] -= delta


for root, subdirectories, files in os.walk(rootdir):
	for filename in files:

		if not filename.endswith('.recording.json'):
			continue

		path = os.path.join(root, filename)

		f = open(path, 'r')
		data = json.load(f)
		f.close()

		cubes = data['scene']

		# ========== check for existence and identify exactly 1 despawned, 2 spawned cubes
		despawn_count = 0
		despawn_time = 0
		despawn_cube = {}
		despawn_point = {}

		spawn_count = 0
		spawn_cubes = []

		for cube in cubes:
			if cube['data'][0]['time'] > 0:
				spawn_count += 1
				spawn_cubes.append(cube)
				continue
			for point in cube['data']:
				if point['z'] <= -100:  # -1000 is height for 'despawned' cubes in c++ code
					despawn_count += 1
					despawn_time = point['time']
					despawn_cube = cube
					despawn_point = last_point
				last_point = point

		if despawn_count != 1 or spawn_count != 2:
			print('DISCARD %s: file does not have exactly 1 despawned and 2 spawned cubes, so i will ignore it.')
			continue

		# ========== test spawned cubes

		cube1 = spawn_cubes.pop()
		cube2 = spawn_cubes.pop()

		c1p = cube1['data'][0]
		c2p = cube2['data'][0]

		c1delta = c1p['time'] - despawn_time
		c2delta = c2p['time'] - despawn_time

		c1dist = math.sqrt((despawn_point['x'] - c1p['x']) ** 2 + (despawn_point['y'] - c1p['y']) ** 2 + (despawn_point['z'] - c1p['z']) ** 2)
		c2dist = math.sqrt((despawn_point['x'] - c2p['x']) ** 2 + (despawn_point['y'] - c2p['y']) ** 2 + (despawn_point['z'] - c2p['z']) ** 2)

		# time difference has to be in (0, 1.5] seconds, euclidean distance in [0, spawned_cube_max_distance] cm -- otherwise discard
		if c1delta > 1.5 or c2delta > 1.5 or (c1delta == 0 and c2delta == 0) or c1dist > spawned_cube_max_distance or c2dist > spawned_cube_max_distance:
			print('DISCARD %s (time or location distance too big or already zero): despawn time: %.2f, spawn times: %.2f (%+.2f, distance %.0f), %.2f (%+.2f, distance %.0f)' % (
				path, despawn_time, cube1['data'][0]['time'], c1delta, c1dist, cube2['data'][0]['time'], c2delta, c2dist))
			continue

		# ========== test despawned cube

		x0 = despawn_cube['data'][0]['x']
		y0 = despawn_cube['data'][0]['y']
		z0 = despawn_cube['data'][0]['z']

		valid = True
		for point in despawn_cube['data']:
			if point['time'] < despawn_time:
				break
			if abs(point['x'] - x0) > despawned_cube_max_movement or abs(point['y'] - y0) > despawned_cube_max_movement or abs(point['z'] - z0) > despawned_cube_max_movement:
				valid = False

		if not valid:
			print('DISCARD %s: despawned cube moved more than %fcm.' % (path, despawned_cube_max_movement))
			continue

		# ========== recording is eligible -> FIX
		fix_despawned_cube(despawn_cube)
		fix_spawned_cube(cube1, c1delta)
		fix_spawned_cube(cube2, c2delta)

		print('FIXED %s: despawn time: %.2f, spawn times: %.2f (%+.2f, distance %.0f), %.2f (%+.2f, distance %.0f)' % (path, despawn_time, cube1['data'][0]['time'], c1delta, c1dist, cube2['data'][0]['time'], c2delta, c2dist))

		# write back to file
		f = open(path, 'w')
		json.dump(data, f, indent=1)
		f.close()
