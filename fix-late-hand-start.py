#!/usr/bin/env python3
import os
import sys
import json
import math

if len(sys.argv) != 2:
	print('''Usage: %s directory
	The directory will be recursively searched for files ending in '.recording.json'.
	Each file is then checked for eligibility and fixed if necessary.
	Check the rules at the start of this script for more info.''' % (sys.argv[0]))
	exit()

name, rootdir = sys.argv


for root, subdirectories, files in os.walk(rootdir):
	for filename in files:

		if not filename.endswith('.recording.json'):
			continue

		path = os.path.join(root, filename)

		f = open(path, 'r')
		data = json.load(f)
		f.close()

		cubes = data['scene']

		needsFixing = False
		delta = 0.0
		for cube in cubes:
			if cube['isActionCube'] and cube['data'][0]['time'] != 0.0:
				print('found candidate: start at %f in %s' %(cube['data'][0]['time'], path))
				needsFixing = True
				delta = cube['data'][0]['time']
				break

		if not needsFixing:
			continue

		for cube in cubes:
			for point in cube['data']:
				if point['time'] - delta >= 0.0:
					#print('from %f to %f' %(point['time'], point['time']-delta))
					point['time'] -= delta


		# write back to file
		f = open(path, 'w')
		json.dump(data, f, indent=1)
		f.close()
