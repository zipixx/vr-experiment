#!/usr/bin/env python3
import os
import sys
import json


if(len(sys.argv) != 2):
	print('Usage: %s dir' %(sys.argv[0]))
	exit()

fileVideoLengthMap = {}

count = 0
totalLength = 0.0

for (root, directories, filenames) in os.walk(sys.argv[1]):
	for filename in filenames:

		if filename.endswith(".recording.json"):
			f = open(os.path.join(root,filename), 'r')
			data = json.loads(f.read())

			for cube in data['scene']:
				if cube['isActionCube']:
					totalLength += cube['data'][len(cube['data'])-1]['time']
					count += 1
					break

print('%i recordings in %s, average length %f' % (count, sys.argv[1], totalLength/count))
