
#!/usr/bin/env python3
import os
import sys
import json
import math

if len(sys.argv) != 2:
	print('''Usage: %s <directory>
	The directory will be recursively searched for files ending in '.recording.json'.
	Files that don't satisfy minimum cube requirement will get printed.
	Check the rules at the start of this script for more info.''' % (sys.argv[0]))
	exit()

name, rootdir = sys.argv

#key has to be part of path of files -> use directory names
specialMinCubes = {'uncover': 4, 'take-down': 4}
defaultMinCubes = 3

ok = 0
bad = 0


for root, subdirectories, files in os.walk(rootdir):
	for filename in files:

		if not filename.endswith('.recording.json'):
			continue

		path = os.path.join(root, filename)

		f = open(path, 'r')
		data = json.load(f)
		f.close()

		cubes = data['scene']

		minCubes = defaultMinCubes

		for key in specialMinCubes:
			if key in path:
				minCubes = specialMinCubes[key]
				break

		if len(cubes) < minCubes:
			bad += 1
			print('bad: %i/%i cubes in %s' %(len(cubes), minCubes, path) )
		else:
			ok += 1

print('ok %i\nbad %i' % (ok, bad) )




