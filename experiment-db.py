#!/usr/bin/env python3

import os
import sys

if(len(sys.argv) != 2):
	print('Usage: %s experiment-result-directory > exp.sql' %(sys.argv[0]))
	exit()


def lines_to_data(lines):
	for i in range(len(lines)):
		line = lines[i].replace('ANSWER: ', '')

		line_entries = line.split(', ')
		data_line = []
		for entry in line_entries:
			data_line.append(entry.split(': ')[1])
		lines[i] = data_line
	return lines


def insert_db(data, cur_id):
	for i in data:
		print("INSERT INTO exp (id, correct, time, guess, action, file) VALUES (%i, '%s', %f, '%s', '%s', '%s');" %(cur_id, i[0], float(i[1]), i[2], i[3], i[4]))
	return



print('DROP TABLE IF EXISTS exp;')
print('CREATE TABLE exp (id int NOT NULL, correct varchar(3) NOT NULL, time numeric(9,6) NOT NULL, guess varchar(20) NOT NULL, action varchar(20) NOT NULL, file varchar(255) NOT NULL);')

cur_id=1
for filename in sorted(os.listdir(sys.argv[1])):
	if filename.endswith(".experiment.txt"):
		f = open(os.path.join(sys.argv[1], filename), 'r')
		lines = f.read().splitlines()
		f.close()
		data = lines_to_data(lines)
		insert_db(data, cur_id)
		cur_id += 1




