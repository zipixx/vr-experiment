#!/usr/bin/env python3
from mysql.connector import (connection)
import subprocess


binSize = 200
octaveFileNameBase = '/tmp/vr_experiment_'


cnx = connection.MySQLConnection(user='', password='', host='localhost', database='test')
cursor = cnx.cursor()

cursor.execute('select distinct(action) from exp;')
actions = [i[0] for i in cursor.fetchall()]

dataGlobal = []

for action in actions:
	#if action != 'Chop' and action != 'Cut': continue
	data = []

	cursor.execute('select distinct(file) from exp where action="' + action + '";')
	files = [i[0] for i in cursor.fetchall()]

	for f in files:
		cursor.execute('select time from exp where file="' + f + '";')
		times = [i[0] for i in cursor.fetchall()]

		timeSum = 0
		timeMin = float('inf')
		timeMax = -1

		for time in times:
			timeSum += time
			if time < timeMin: timeMin = time
			if time > timeMax: timeMax = time

		for time in times:
			relTime = time - timeSum / len(times)
			if abs(relTime) < 10.0:
				data.append(relTime)

	dataGlobal.extend(data)

	command = 'data = [%s];\nhist(data, %i, "", [-10 10]);' %(' '.join([str(i) for i in data]), binSize)
	f = open(octaveFileNameBase + action + '.m', "w")
	f.write(command)
	f.close()


command = 'data = [%s];\nhist(data, %i, "", [-10 10]);' %(' '.join([str(i) for i in dataGlobal]), binSize)
f = open(octaveFileNameBase + 'COMBINED.m', "w")
f.write(command)
f.close()

