import sys
import os
import subprocess


#step1: ffmpeg -hide_banner -loglevel panic -ss 0 -t 2 -i videos/lay.mp4 -c copy tmp.mp4
#step2: run this with base_delay from step1 video (change iMax var below for faster tests)
#step3: check for errors
#helpful:
#ffmpeg -hide_banner -loglevel panic -ss 00:00:09.0 -i videos-out/lay/2018-08-23_13-05-17.recording.mp4 -c copy tmp.mp4
#ffmpeg -hide_banner -loglevel panic -ss 00:00:01.0 -i videos-out/lay/2018-08-26_14-29-05.recording.mp4 -t 00:00:09.8 -c copy tmp.mp4


if len(sys.argv) < 5 or '.mp4' not in sys.argv[1] or '.txt' not in sys.argv[2] or not os.path.isdir(sys.argv[3]):
	print('Usage: %s videos/chop.mp4 videos/chop.txt videos-out/ base-delay\nbase-delay as offset from first scene spawning and 1 second video timestamp.' %(sys.argv[0]))
	exit()

fullSourceVideo = sys.argv[1]
resultDirectory = sys.argv[3]
baseDelay = float(sys.argv[4])

f = open(sys.argv[2], 'r')
lines = f.read().splitlines()

n = len(lines)

timeList = []
fileList = []

iMax = 30
i = 0

for line in lines:
	if i==iMax:
		break;
	columns = line.split()
	timeList.append(float(columns[3]))
	fileList.append(columns[4])
	i+=1

for i in range(0, iMax-1):
	name = resultDirectory + fileList[i][fileList[i].rfind('/'):fileList[i].rfind('.')] + '.mp4'
	start = timeList[i] + 0.5
	end = timeList[i+1] - 1.5

	print('ffmpeg -hide_banner -loglevel panic -ss 00:%s:%f -i %s -c copy -t 00:%s:%f %s' % (str(int((baseDelay+start)/60)).zfill(2), (baseDelay+start)%60, fullSourceVideo, str(int((end-start)/60)).zfill(2), end-start, name))
	subprocess.call('ffmpeg -hide_banner -loglevel panic -ss 00:%s:%f -i %s -c copy -t 00:%s:%f %s' % (str(int((baseDelay+start)/60)).zfill(2), (baseDelay+start)%60, fullSourceVideo, str(int((end-start)/60)).zfill(2), end-start, name), shell=True)

#TODO rm if
if iMax >= n:
	#last index -> til end
	name = resultDirectory + fileList[n-1][fileList[i].rfind('/'):fileList[i].rfind('.')] + '.mp4'
	start = timeList[n-1] + 0.25
	print('ffmpeg -hide_banner -loglevel panic -ss 00:%s:%f -i %s -c copy %s' % (str(int((baseDelay+start)/60)).zfill(2), (baseDelay+start)%60, fullSourceVideo, name))
	subprocess.call('ffmpeg -hide_banner -loglevel panic -ss 00:%s:%f -i %s -c copy %s' % (str(int((baseDelay+start)/60)).zfill(2), (baseDelay+start)%60, fullSourceVideo, name), shell=True)




#print(timeList)
#print(fileList)



