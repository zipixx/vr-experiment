#!/usr/bin/env python3
from mysql.connector import (connection)
import subprocess
import os
import sys
import json
import statistics


# put the recordings into ./exp/ (-> same relative path as from _DATA on ue4 vr system)
# modify mysql user config below and fill db with "mysql -u vr-user -p vr < exp.sql"
# this prints a html table -> "mkdir -p /tmp/vr && ./results-table.py > /tmp/vr/_table.html && xdg-open /tmp/vr/_table.html"

samplesPerAction = 30


fileVideoLengthMap = {}

for (root, directories, filenames) in os.walk('exp/'):
	for filename in filenames:

		if filename.endswith(".recording.json"):
			f = open(os.path.join(root,filename), 'r')
			data = json.loads(f.read())

			for cube in data['scene']:
				if cube['isActionCube']:
					fileVideoLengthMap[ os.path.join(root,filename) ] = cube['data'][len(cube['data'])-1]['time']
					break


cnx = connection.MySQLConnection(user='vr-user', password='ew5ohvakeiPhothi', host='localhost', database='vr')
cursor = cnx.cursor()

cursor.execute('select distinct(id) from exp order by id asc;')
subjects = [i[0] for i in cursor.fetchall()]

cursor.execute('select distinct(action) from exp order by action asc;')
actions = [i[0] for i in cursor.fetchall()]

print('<html><head><style>table{width: 800px; table-layout: fixed; border-collapse: collapse;} td{text-align: center;vertical-align: middle;} .average,.title{background-color:#ccc;} #totalAverage{background-color:#999;}</style></head><body><table border=1>')
print('<tr class="title"><th>Subject#</th>', end='')

actionPredictabilityDict = {}
for action in actions:
	actionPredictabilityDict[action] = []
	print('<th>%s</th>' % action, end='')
#print('Average')
print('<th>Average</th></tr>')


totalAveragePredictabilityBySubject = []

for subject in subjects:

	print('<tr><td class="title">%i</td>' % subject, end='')

	powerBySubject = []

	for action in actions:

		subjectsPowerByAction = []
		cursor.execute('select correct, time, action, file from exp where id=%i and action="%s";' % (subject, action) )

		for row in cursor:
			correct, time, action, filename = row
			if(correct == 'no'):
				continue

			predictabilityPower = (1 - float(time) / fileVideoLengthMap[filename]) * 100
			predictabilityPower = max(0.0, min(predictabilityPower, 100.0))
			subjectsPowerByAction.append( predictabilityPower )

		averagePowerByAction = sum(subjectsPowerByAction)/len(subjectsPowerByAction)
		powerBySubject.append(averagePowerByAction)
		actionPredictabilityDict[action].append(averagePowerByAction)

		print('<td>%s</td>' % "{0:.2f}".format(averagePowerByAction), end='')

	averagePowerBySubject = sum(powerBySubject)/len(powerBySubject)
	totalAveragePredictabilityBySubject.append(averagePowerBySubject)
	#print('powerBySubject: %i - %f - STD %f' %(subject, averagePowerBySubject, statistics.stdev(powerBySubject)) )
	print('<td class="average">%s</td></tr>' % "{0:.2f}".format(averagePowerBySubject))


print('<tr class="average"><td>Average</td>', end='')
for action in actions:
	value = actionPredictabilityDict[action]
	print('<td>%s</td>' % "{0:.2f}".format(sum(value) / len(value)), end='')
	#print('%s - %f - STD %f' %(key, sum(value) / len(value), statistics.stdev(value)))

#print('totalAveragePredictabilityBySubject: %f - STD %f' %(sum(totalAveragePredictabilityBySubject)/len(totalAveragePredictabilityBySubject), statistics.stdev(totalAveragePredictabilityBySubject)) )
print('<td id="totalAverage">%s</td></tr>' % "{0:.2f}".format(sum(totalAveragePredictabilityBySubject)/len(totalAveragePredictabilityBySubject)) )

print('<tr><td class="title">STD</td>', end='')
for action in actions:
	value = actionPredictabilityDict[action]
	print('<td>%s</td>' % "{0:.2f}".format(statistics.stdev(value)), end='')

print('<td class="average">%s</td></tr>' % "{0:.2f}".format(statistics.stdev(totalAveragePredictabilityBySubject)) )


print('<tr><td class="title">Correct</td>', end='')
cursor.execute('select action, count(*)/%i *100 as correctPercentage from exp where correct="yes" group by action order by action asc;'%(len(subjects)*samplesPerAction))
percentageSum = 0
for percent in [i[1] for i in cursor.fetchall()]:
	print('<td>%s</td>' % "{0:.2f}".format(percent), end='')
	percentageSum += percent

print('<td class="average">%s</td></tr>' % "{0:.2f}".format(percentageSum/len(actions)))


print('</table></body></html>')




