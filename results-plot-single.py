#!/usr/bin/env python3
import numpy as np
import matplotlib.pyplot as plt
from mysql.connector import (connection)
from collections import OrderedDict
import json
import sys
import os


samplesPerAction = 30
resultDirectory = '/tmp/vr/'

videoLengthMap = {}

for (root, directories, filenames) in os.walk('exp/'):
	for filename in filenames:
		if filename.endswith(".recording.json"):
			f = open(os.path.join(root,filename), 'r')
			data = json.loads(f.read())
			for cube in data['scene']:
				if cube['isActionCube']:
					videoLengthMap[ os.path.join(root,filename) ] = cube['data'][len(cube['data'])-1]['time']
					break



def plotAction(action, subjects):
	correctList = []
	wrongList = []
	plt.figure(figsize=(1400/100, 500/100), dpi=100)
	fig, ax1 = plt.subplots()

	separatorOffset = 1/samplesPerAction*0.5
	count = 0.5 + separatorOffset #points
	for subject in subjects:
		ax1.axvline(x=(count - separatorOffset), color='black') #seperator
		cursor.execute('select correct, time, file from exp where id=%i and action="%s" order by file asc;' % (subject, action) )

		pPowerSum = 0
		for exp in cursor:
			correct, time, filename = exp
			predictabilityPower = (1 - float(time) / videoLengthMap[filename]) * 100
			predictabilityPower = np.clip(predictabilityPower, 0.0, 100.0)
			pPowerSum += predictabilityPower

			if(correct == 'yes'):
				correctList.append([count, predictabilityPower])
			else:
				wrongList.append([count, predictabilityPower])

			count += 1/samplesPerAction
		plt.hlines(y=(pPowerSum/samplesPerAction), xmin=(count - 1 - separatorOffset), xmax=(count - separatorOffset), color='gray', label='correct average' ) #subject average

	ax1.axvline(x=(count - separatorOffset), color='black')


	data = np.array(correctList)
	x, y = data.T
	ax1.scatter(x, y, 10, edgecolors='none', color='blue', label='correct')

	data = np.array(wrongList)
	x, y = data.T
	ax1.scatter(x, y, 10, edgecolors='none', color='red', label='incorrect')


	plt.title('%s'%action, fontsize=18)
	ax1.set_xbound(0.5, len(subjects)+0.5)
	plt.xticks(np.arange(1, len(subjects)+1, 1))
	ax1.set_ybound(0, 100)
	plt.yticks(np.arange(0, 101, 10))
	plt.xlabel('Human Subject (%i Samples Each)' % (samplesPerAction), fontsize=12)
	plt.ylabel('Predictability Power (%)', fontsize=12)


	#unique labels & show
	handles, labels = plt.gca().get_legend_handles_labels()
	by_label = OrderedDict(zip(labels, handles))
	plt.legend(by_label.values(), by_label.keys())
	#plt.show()
	plt.savefig('%s/%s.png' % (resultDirectory, action.replace(' ', '-').lower()), dpi=250, bbox_inches='tight')



cnx = connection.MySQLConnection(user='vr-user', password='ew5ohvakeiPhothi', host='localhost', database='vr')
cursor = cnx.cursor()

cursor.execute('select distinct(id) from exp order by id asc;')
subjects = [i[0] for i in cursor.fetchall()]

cursor.execute('select distinct(action) from exp order by action asc;')
actions = [i[0] for i in cursor.fetchall()]

if not os.path.exists(resultDirectory):
    os.makedirs(resultDirectory)

for action in actions:
	plotAction(action, subjects)





